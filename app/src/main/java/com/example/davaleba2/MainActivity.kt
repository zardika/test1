package com.example.davaleba2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    private lateinit var resultTextView: TextView
    private var operand:Double = 0.0
    private var operation = ""
    //private var addOperation = false
    //private var addDecimal = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        resultTextView = findViewById((R.id.resultTextView))
    }
    fun numberClick(clickedView: View){
        if (clickedView is TextView){
            var result = resultTextView.text.toString()
            val number = clickedView.text.toString()
            if (result == "0"){
                result = ""
            }

            resultTextView.text = result + number
            
        }
    }
    fun operationClick(clickedView: View){
        if (clickedView is TextView){
            var operand = resultTextView.text.toString()
            this.operand = operand.toDouble()
            operation = clickedView.text.toString()
            //resultTextView.append(clickedView.text)
            resultTextView.text = ""

        }
    }
    fun equalsClick(clickedView: View){
        if (clickedView is TextView){
        val secOperant = resultTextView.text.toString().toDouble()
        when(operation) {
            "+" -> resultTextView.text = (operand + secOperant).toString()
            "-" -> resultTextView.text = (operand - secOperant).toString()
            "*" -> resultTextView.text = (operand * secOperant).toString()
            "/" -> resultTextView.text = (operand / secOperant).toString()
          }
        }
    }

    fun delClick(clickedView: View) {
        if (clickedView is TextView) {
            val length = resultTextView.length()
            if (length > 0 ){
                resultTextView.text = resultTextView.text.subSequence(0 , length -1)
            }

        }
    }
}
